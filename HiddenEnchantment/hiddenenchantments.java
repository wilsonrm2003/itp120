import java.util.*;
import java.io.*;
class hiddenenchantments {
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		ArrayList<String> linesOfFile = new ArrayList<String>();
		while (scanner.hasNextLine()){
			String line = scanner.nextLine();
			String a = new String(line);
			linesOfFile.add(a);
		}
		String bookPassage = linesOfFile.get(0);
		String enchantment = linesOfFile.get(1);
		int passageLength = bookPassage.length();
		int enchcantmentLength = enchantment.length();
		System.out.println("  " + bookPassage);
		for(int i = passageLength; i > 0; i-= 1){
			System.out.print("-");
		}
		System.out.println("");
		String enchant[] = enchantment.split("");
		List<String> enchantSplit = new ArrayList<String>();
		enchantSplit = Arrays.asList(enchant);
		String book[] = bookPassage.split("");
		List<String> passageSplit = new ArrayList<String>();
		passageSplit = Arrays.asList(book);
		for(String ench : enchantSplit){
			String writingEnchantment = enchantment;
			int writingLength = writingEnchantment.length();
			for (int i = writingLength; i > 0; i -= 1){
				for(String pass : passageSplit){
					if(pass == ench){
						writingEnchantment = writingEnchantment.replace(pass, "!");
					}else{	
						writingEnchantment = writingEnchantment.replace(pass, ".");	
					}
				}	
			}
			System.out.println(ench + "|" + writingEnchantment);
		}	 
	}

}
