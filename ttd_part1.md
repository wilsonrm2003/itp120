What are the Three Laws of Test-driven Development?
only write production code to pass a failing test
only write enough of a test to demonstrate failure
only write enough production code to pass the failing test

Explain the Red -> Green -> Refactor -> Red process.
a TTD procces where you write the failing test(red) then write the production code(green) then add more to the test and continue the process

What are the three characteristics Uncle Bob lists of rotting code? (Note: these were covered in Episode 1).
rigid, fragile, and immobile

Explain how fear promotes code rot and why the fear exists in the first place. How does TDD help us break this vicious cycle?
when trying to fix a broken code you may add more and more to break it and rot it down to something that does not make sense and the more you add the more your code rots

Uncle Bob mentions FitNesse as an example of a project that uses TDD effectively. What is FitNesse? What does it do?
FitNesse is a web based tool used for software testing, and is open source.

What does Uncle Bob say about a program with a long bug list? What does he say this comes from?
a program that has a long bug list shows a programmer is careless and irresponsible

What two other benefits does Uncle Bob say you get from TDD besides a reduction in debugging time?
loss of fear from changing code and keeps defects under control.
