import java.awt.*;
import javax.swing.*;
class gradientCircle extends JPanel{
	public void paintComponent(Graphics g){
		Graphics2D g2d = (Graphics2D) g;
		GradientPaint gradient = new GradientPaint(70, 70, Color.blue, 150, 150, Color.red);
		g2d.setPaint(gradient);
		g2d.fillOval(70, 70, 100, 100);
	}
}
