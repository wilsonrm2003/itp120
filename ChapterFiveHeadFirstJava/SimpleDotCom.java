class SimpleDotCom{
    private int[] locations;
    private int hits = 0;
    private int guesses = 0;
    public SimpleDotCom(){
        int start = (int)(Math.random() * 5);
        this.locations = new int[] {start, start + 1, start + 2};
    }
    public void setLocationCells(int[] setLocations) {
        locations = setLocations;
    }
    public boolean over(){
        return this.hits > 2;
    }
    public String makeMove(String userGuess){
        int guess = Integer.parseInt(userGuess);
        this.guesses++;
        for (int location : locations){
            if (location == guess){ 
                this.hits++;
                if (this.hits > 2){
                    return "kill";
                }
                return "hit";
            }
        }
    }
        return "miss";
    }
    public String displayResults(){
        return "You took " + this.guesses + " guesses.";
    }
}
