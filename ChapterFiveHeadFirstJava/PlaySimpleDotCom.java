public class PlaySimpleDotCom {
    public static void main(String[] args){
        CliReader reader = new CliReader();
        SimpleDotCom game = new SimpleDotCom();
        while (!game.over()){
            System.out.println(game.makeMove(reader.input("Enter a number: ")));
        }
        System.out.println(game.displayResults());
    }
}  
