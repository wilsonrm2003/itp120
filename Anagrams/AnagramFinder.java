import java.util.Scanner;
import java.util.ArrayList;

public class AnagramFinder {

    ArrayList<String> anagrams = new ArrayList<String>();
    ArrayList<String> dictionary = new ArrayList<String>();
    int[][] dChars;

    public void AnagramFinder() {
        Scanner scanner = new Scanner(System.in);
        String inputSentence = scanner.nextLine();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            dictionary.add(line);
        }

        int[] iChar = amountOfCharactersIn(inputSentence);
        /* an array of 26 values that correspond to
        the frequency of each of the 26 letters */

        int length = dictionary.size();
        dChars = new int[length][26];
        //dictionary character frequency

        for (int i = 0; i < length; i++) {
            dChars[i] = amountOfCharactersIn(dictionary.get(i));
        }

        //ArrayList<String> anagram = new ArrayList<String>();

        //anagram.add(dictionary[0]);
        int[] emptyArray = new int[26];
        search("", emptyArray, 0, iChar);

        for (int i = 0; i < anagrams.size(); i++) {
            System.out.println(anagrams.get(i));
        }

        if (anagrams.isEmpty()) {
            System.out.println("No anagrams for this sentence");
        }

    }

    public int[] amountOfCharactersIn(String string) {
        int[] result = new int[26];
        for (char c : string.toCharArray()) {
            if (!(c == ' ')) {
                result[c-97]++;
            }
        }
        return result;
    }

    public int[] addCharLists(int[] list1, int[] list2) {
        for (int i = 0; i < 26; i++) {
            list1[i]+= list2[i];
        }
        return list1;
    }

    public String checkifEqual(int[] list1, int[] list2) {
        for (int i = 0; i < 26; i++) {
            if (!(list1[i] == list2[i])) {
                if (list1[i] > list2[i]) {
                    return "too many";
                }
                return "not enough";
            }
        }
        return "fit";
    }


    public String search(String anagram, int[] charList, int place, int[] iChar) {

        for (int i = place; i < dChars.length; i++) {
            int[] added = addCharLists(charList, dChars[i]);
            String check = checkifEqual(added, iChar);
            if (check == "fit") {
                anagrams.add(anagram + dictionary.get(place));
                return "end";
            } else if (check == "too many") {

            } else {
                if (search(anagram + dictionary.get(place) + " ", added, place + 1, iChar) == "fit"){
                    break;
                }
            }
        }
        return "search complete";
    }
}

