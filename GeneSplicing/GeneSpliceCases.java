import java.util.Scanner;
import java.lang.Math;

class GeneSpliceCases {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int length = Integer.parseInt(scanner.nextLine());
        int[] A = new int[length];
        int[] B = new int[length];
        int[] C = new int[length];
        double[] T = new double[length];

        int i = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] stringArray = line.split(" ");
            A[i] = Integer.parseInt(stringArray[0]);
            B[i] = Integer.parseInt(stringArray[1]);
            C[i] = Integer.parseInt(stringArray[2]);
            T[i] = Double.parseDouble(stringArray[3]);
            i++;
        }

        for (i = 0; i < length; i++) {
            double testC = (double) C[i];

            double virusAin1 = (double) A[i];
            double virusAin2 = 0;
            double virusBin1 = 0;
            double virusBin2 = (double) B[i];

            double idealRatio = virusAin1/virusBin2;
            double ratio = 0;

            int j = 0;
            while (!(Math.abs(ratio - idealRatio) < T[i])) {
                double total1 = virusAin1 + virusBin1;
                double transferredA = (virusAin1/total1) * testC;
                double transferredB = (virusBin1/total1) * testC;
                virusAin1 -= transferredA;
                virusBin1 -= transferredB;
                virusAin2 += transferredA;
                virusBin2 += transferredB;
                ratio = virusAin2/virusBin2;

                double total2 = virusAin2 + virusBin2;
                transferredA = (virusAin2/total2) * testC;
                transferredB = (virusBin2/total2) * testC;
                virusAin2 -= transferredA;
                virusBin2 -= transferredB;
                virusAin1 += transferredA;
                virusBin1 += transferredB;
                ratio = virusBin1/virusAin1;
                ratio = virusAin2/virusBin2;

                //float temp1 = virusAin1;
                //float temp2 = virusBin1;
                //virusAin1 = virusAin2;
                //virusBin1 = virusBin2;
                //virusAin2 = temp1;
                //virusBin2 = temp2;
                j++;
            }
            System.out.println(j);
        }
    }
}
