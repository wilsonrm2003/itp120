import java.util.*;
import java.io.*;
 
class QuittitchScanner{ 	
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		String[] houseArray = new String[4];
		houseArray[0] = "Gryffindor";
		houseArray[1] = "Ravenclaw";
		houseArray[2] = "Slytherin";
		houseArray[3] = "Hufflepuff";
		ArrayList<Integer> numOfPlayers = new ArrayList<Integer>();
		ArrayList<String> linesOfFile = new ArrayList<String>();
		while (scanner.hasNextLine()){
			String line = scanner.nextLine();
			String a = new String(line);
			linesOfFile.add(a);//adds each line of the .txt file to the linesOfFile arraylist
		}
		for(String house : houseArray){
			int players = 0;
			for (String line : linesOfFile){
				if(line.contains(house)){//looks at which lines have specific houses
					players++;//counts the number of lines in the file that have the house
				}
			}
			numOfPlayers.add(players);//counts number of students for each class
		}
		ArrayList<Integer> checker = new ArrayList<Integer>();
		for(int e = 0; e < 4; e++){
			checker.add(7);//checks the houses
		}
		if(numOfPlayers.equals(checker)){
			System.out.println("List complete, let's play quiddich!");
			return;
		}
		for (int i = 0 ; i < 4; i++){
			if(numOfPlayers.get(i) < 7){
				System.out.print(houseArray[i] + " does not have enough players.");
				//shows what house is missing players
			}
			else if(numOfPlayers.get(i) < 7){
				System.out.println(houseArray[i] + " has too many players.");
				//shows what house has too many players
			}
			else{
				System.out.println(houseArray[i] + " has seven players exactly.");
				//shows what house has the right number of players
			}
		}
	}
} 
//Credit Leila Meng
//I got too distracted over the weekend and failed to do the assignment so I looked over Leila's and could not think of a different solution. 
